<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cms;
use AppBundle\Entity\CmsDb;
use AppBundle\Entity\Genus;
use AppBundle\Helper\Site;
use AppBundle\Helper\Imprint;
use AppBundle\Helper\Language;
use AppBundle\Helper\Title;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class DefaultController extends Controller
{

    protected $_imprintHelper;
    protected $_siteHelper;
    protected $_language;

    public function __construct(Imprint $imprintHelper, Site $siteHelper, Language $language)
    {
        $this->_imprintHelper = $imprintHelper;
        $this->_siteHelper = $siteHelper;
        $this->_language = $language;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     *@Route("/{value}")
     */
    public function showAction($value)
    {
        // wenn es ein Land ist de / en / nl ...
        if($this->_language->isLanguage($value)) {
            $cmsRow = $this->_siteHelper->getCmsRow($value,$this->getEm(),'cms');

            return $this->render(
                'default/site.html.twig', [
                'title' => $this->_siteHelper->getTitle($cmsRow),
                'content' => $this->_siteHelper->getContent($cmsRow),
                'linktext' => $this->_siteHelper->getLinktext($cmsRow),
                'url' => '/' . $value . '/imprint',
            ]);
        } else {
            die('not know url');
        }
    }


    /**
     * @Route("/{language}/{value}")
     */
    public function imprintAction(Request $request,$language,$value)
    {
        if ($value == Imprint::imprint) {
            $cmsRow = $this->_siteHelper->getCmsRow($language,$this->getEm(),'imprint');

            return $this->render(
                'default/imprint.html.twig',[
                'imprint_title' => $this->_siteHelper->getTitle($cmsRow),
                'imprint_content' => $this->_siteHelper->getContent($cmsRow),
            ]);
        } else if($value == 'edit') {
            return $this->editAction($request,$language,'imprint');
        }

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @param Request $request
     * @param $language
     * @return Response
     */
    public function editAction(Request $request, $language, $value)
    {

        $entityManager = $this->getEm();
        $cms = $this->_siteHelper->getCmsRow($language,$entityManager,$value);

        $form = $this->createFormBuilder($cms[0])
            ->add('title', TextType::class)
            ->add('content', TextType::class)
            ->add('language',TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Save'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $cms = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            // $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($cms);
             $entityManager->flush();

            $this->redirect("/");

            return new Response('Imprint update successfuly');
        }

                return $this->render('admin/edit/imprint.html.twig', [
            'form' => $form->createView(),
        ]);

//        // ================================
//        $em = $this->getEm();
//        $cms = $this->_siteHelper->getCmsRow($language,$em,$value);
//        dump($cms);
////        $cms->setTitle($this->_siteHelper->getTitle($cms));
////        $cms->setContent($this->_siteHelper->getContent($cms));
////        $cms->setLanguage($language);
////        $cms->setTypeid('imprint');
//
//
//        $form = $this->createFormBuilder($cms)
//            ->add('title', TextType::class)
//            ->add('content', TextType::class)
//            ->add('language',TextType::class)
//            ->add('save', SubmitType::class, ['label' => 'Save'])
//            ->getForm();
//
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//
//            $em->flush();
//
//            $this->redirect("/");
//
//            return new Response('Imprint update successfuly');
//        }
//
//        return $this->render('admin/edit/imprint.html.twig', [
//            'form' => $form->createView(),
//        ]);


    }

    /**
     * @param Request $request
     * @param $language
     * @param $value
     * @return Response
     */
    public function insertAction(Request $request, $language, $value)
    {
        $cms = new Cms();

        $form = $this->createFormBuilder($cms)
            ->add('title', TextType::class)
            ->add('content', TextType::class)
            ->add('language',TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Save'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getEm();
            $em->persist($cms);
            $em->flush();

            $this->redirect("/");

            return new Response('Imprint added successfuly');
        }

        return $this->render('admin/edit/imprint.html.twig', [
            'form' => $form->createView(),
        ]);


    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    public function getEm() {
        return $this->getDoctrine()->getManager();
    }

}
