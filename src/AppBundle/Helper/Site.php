<?php

namespace AppBundle\Helper;

use AppBundle\Entity\Cms as cmsHelper;
use AppBundle\Entity\Cms;
use AppBundle\Entity\CmsDb;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class Site extends Controller
{
    /**
     * @param $language Language
     * @param $em \Doctrine\Common\Persistence\ObjectManager
     * @param $typeid
     * @return mixed
     */
    public function getCmsRow($language,$em,$typeid) {
        return $em->getRepository('AppBundle:Cms')->findBy( array('typeid' => $typeid,'language' => $language));
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getTitle($row) {
        if(is_array($row)) {
            $cms = $row[0];
            return $cms->getTitle();
        }
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getContent($row) {
        if(is_array($row)) {
            $cms = $row[0];
            return $cms->getContent();
        }
    }

    public function getLinktext($row) {
        if(is_array($row)) {
            $cms = $row[0];
            return $cms->getLinktext();
        }
    }
}
