<?php

namespace AppBundle\Helper;

class Language
{
    /* Language Constanten */
    const de = 'de';
    const en = 'en';
    const nl = 'nl';

    /**
     * @param $language
     * @return bool
     */
    public function isLanguage ($language) {
        if($language == Language::de || $language == Language::en || $language == Language::nl) {
            return true;
        }

        return false;
    }
}
