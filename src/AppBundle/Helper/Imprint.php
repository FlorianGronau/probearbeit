<?php

namespace AppBundle\Helper;

class Imprint
{
    const imprint = 'imprint';

    protected $file = '_imprint.csv';

    /**
     * @var $title
     */
    protected $title;

    /**
     * @var $content
     */
    protected $content;

    /**
     * @var $language
     */
    protected $language;


    public function setLanguage($language) {
        $this->language = $language;
    }

    public function getLanguage() {
        return $this->language;
    }


    public function setTitle($title) {
        $this->title = $title;
    }

    public function getTitle() {
        return $this->title;
    }


    public function setContent($content) {
        $this->content = $content;
    }

    public function getContent() {
        return $this->content;
    }


//    public function setImprintContent($value, $language) {
//        $cmsParts = explode(";", $this->getFileByLanguage($language) );
//       $this->writeFile( $cmsParts[0] . ';' .  $value, $language);
//
//    }
//
//    public function setImprintTitle($value, $language) {
//        $cmsParts = explode(";", $this->getFileByLanguage($language) );
//        $this->writeFile(  $value . ';' . $cmsParts[1] , $language);
//    }
//
//    public function writeFile($value,$language) {
//        file_put_contents($language. $this->file, $value);
//    }

//    /**
//     * @param $language
//     * @return string
//     */
//    public function getImprintTitle($language) {
//        $cmsParts = explode(";", $this->getFileByLanguage($language) );
//        return $cmsParts[0];
//    }
//
//    /**
//     * @param $language
//     * @return string
//     */
//    public function getImprintContent($language) {
//        $cmsParts = explode(";", $this->getFileByLanguage($language) );
//        return $cmsParts[1];
//    }

//    /**
//     * @param $language
//     * @return false|string
//     */
//    public function getFileByLanguage($language) {
//        return file_get_contents('./'.$language.$this->file);
//    }
}
