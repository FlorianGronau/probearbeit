<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="cms")
 */
class Cms
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $typeid;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $linktext;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $linktext
     * @return $this
     */
    public function setLinktext($linktext)
    {
        $this->linktext = $linktext;
        return $this;
    }

    /**
     * @return string
     */
    public function getLinktext()
    {
        return $this->linktext;
    }

    /**
     * Set type
     *
     * @param string $typeid
     *
     * @return Cms
     */
    public function setTypeid($typeid)
    {
        $this->typeid = $typeid;
        return $this;
    }

    /**
     * Get Type
     *
     * @return string
     */
    public function getTypeid()
    {
        return $this->typeid;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Cms
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Cms
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return CmsDb
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

}