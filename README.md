Probearbeit
===========

A Symfony project created on April 9, 2019, 7:49 pm.

###Aufgabe 1 :

* Setze eine Sympfony Application auf. (v3.4)
* in der App soll es 3 Protale geben:
    DE / EN / NL
* Jedes Protal hat eine Impressum-Seite
   (localhost/de/impressum)
   (localhost/en/imprint)
   (localhost/nl/afdruk)
   Unter diesem Link wird nur der Inhalt zur passenden Sprache ausgegeben.
   
* Es soll eine Pflegemöglichkeit geschaffen werden, in welcher das Impressum für jeweils das Portal angelegt bzw. geändert werden kann.
   
### Aufgabe 2
   
* In die bestehende Symfony-App soll nun eine User-Übersicht eingebaut werden. Diese soll über localhost/users aufrufbar sein.
   
* Beim Aufruf werden die User über einen Service von einer API abgerufen.
   https://isonplaceholder.typicode.com/users. Für den Abruf der Daten kann Guzzle verwendet werden.
   
* Anschließend werden die einzelnen User in Value Objekte konvertiert und tabellarisch ausgegeben.
   
* Bonus: Die daten aus Aufgabe 1 können über eine API abgerufen werden.
   
* Hinweiß: Auf Disign muss nicht geachtet weden. Gerne darf aber natürlich z.B Bootstrap verwendet werden. 
   
   * Im Controller soll keine "Bussiness" Logik sein